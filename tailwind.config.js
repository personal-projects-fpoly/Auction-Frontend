/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        red: "#B41712",
        blue: "#2566E4",
      },
      zIndex: {
        "-1": "-1",
        "-10": "-10",
      },
    },
  },
  plugins: [
    function ({ addUtilities }) {
      addUtilities({
        ".clamp-1": {
          display: "-webkit-box",
          "-webkit-line-clamp": "1",
          "-webkit-box-orient": "vertical",
          overflow: "hidden",
          "text-overflow": "ellipsis",
          "word-break": "break-word",
        },
        ".no-scrollbar": {
          "&::-webkit-scrollbar": {
            display: "none",
          },
          "-ms-overflow-style": "none",
          "scrollbar-width": "none",
        },
      });
    },
  ],
};
