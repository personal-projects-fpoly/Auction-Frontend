import { CanActivateFn } from '@angular/router';

export const guardGuard: CanActivateFn = (route, state) => {
  const user = sessionStorage.getItem('user');

  if (!user) {
    window.location.href = '/';
    return false;
  }
  let userData;
  try {
    userData = JSON.parse(user);
  } catch (error) {
    console.error('Lỗi khi parse user từ sessionStorage:', error);
    return false;
  }

  // Kiểm tra vai trò của user
  if (userData.role !== 'admin') {
    window.location.href = '/';
    return false;
  }

  // Nếu mọi thứ đều ổn, cho phép truy cập
  return true;
};
