import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientHomeComponent } from './pages/client/home/home.component';
import { ClientComponent } from './pages/client/client.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AdminDashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { AdminProductComponent } from './pages/admin/product/product.component';
import { AdminProductListComponent } from './pages/admin/product/components/product-list/product-list.component';
import { AdminProductAddComponent } from './pages/admin/product/components/product-add/product-add.component';
import { AdminProductEditComponent } from './pages/admin/product/components/product-edit/product-edit.component';
import { AdminCategoryListComponent } from './pages/admin/category/category-list/category-list.component';
import { AdminCategoryAddComponent } from './pages/admin/category/category-add/category-add.component';
import { AdminCategoryEditComponent } from './pages/admin/category/category-edit/category-edit.component';
import { AdminCategoryComponent } from './pages/admin/category/category.component';
import { ClientSignupComponent } from './pages/client/signup/signup.component';
import { ClientDetailComponent } from './pages/client/detail/detail.component';
import { ClientListComponent } from './pages/client/list/list.component';
import { ClientContactComponent } from './pages/client/contact/contact.component';
import { ClientAboutComponent } from './pages/client/about/about.component';
import { AdminConversationComponent } from './pages/admin/conversation/conversation.component';
import { AdminMessageTextingComponent } from './pages/admin/conversation/components/message-texting/message-texting.component';
import { AdminMessageLabelComponent } from './pages/admin/conversation/message-label/message-label.component';
import { AdminMessageStatusComponent } from './pages/admin/conversation/message-status/message-status.component';
import { AdminMessageStarComponent } from './pages/admin/conversation/message-star/message-star.component';
import { AdminMessagesComponent } from './pages/admin/conversation/messages/messages.component';
import { guardGuard } from './guard/guard.guard';
import { ClientNewsComponent } from './pages/client/news/news.component';
import { ClientCheckoutComponent } from './pages/client/checkout/checkout.component';
import { ClientAccountComponent } from './pages/client/account/account.component';
import { ClientAccountInfomationComponent } from './pages/client/account/components/account-infomation/account-infomation.component';
import { ClientDepositComponent } from './pages/client/account/components/deposit/deposit.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      { path: '', component: ClientHomeComponent },
      { path: 'list', component: ClientListComponent },
      { path: 'contact', component: ClientContactComponent },
      { path: 'news', component: ClientNewsComponent },
      { path: 'about', component: ClientAboutComponent },
      { path: 'signup', component: ClientSignupComponent },
      { path: 'detail/:id', component: ClientDetailComponent },
      { path: 'checkout/:id', component: ClientCheckoutComponent },
      {
        path: 'account',
        component: ClientAccountComponent,
        children: [
          { path: '', component: ClientAccountInfomationComponent },
          {
            path: 'deposit',
            component: ClientDepositComponent,
          },
        ],
      },
    ],
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [guardGuard],
    children: [
      { path: '', component: AdminDashboardComponent },
      {
        path: 'product',
        component: AdminProductComponent,
        children: [
          { path: '', component: AdminProductListComponent },
          { path: 'add', component: AdminProductAddComponent },
          { path: 'edit/:id', component: AdminProductEditComponent },
        ],
      },
      {
        path: 'category',
        component: AdminCategoryComponent,
        children: [
          { path: '', component: AdminCategoryListComponent },
          { path: 'add', component: AdminCategoryAddComponent },
          { path: 'edit', component: AdminCategoryEditComponent },
        ],
      },
      {
        path: 'conversation',
        component: AdminConversationComponent,
        children: [
          {
            path: '',
            component: AdminMessagesComponent,
          },
          {
            path: 'star',
            component: AdminMessageStarComponent,
          },
          {
            path: 'label/:id',
            component: AdminMessageLabelComponent,
          },
          {
            path: 'status/:id',
            component: AdminMessageStatusComponent,
          },
          {
            path: 'texting/:id',
            component: AdminMessageTextingComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
