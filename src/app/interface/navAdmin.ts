export interface AdminMenuItem {
  label: string;
  icon?: string;
  route?: string;
  active?: boolean;
  expanded?: boolean;
  children?: AdminMenuItem[];
}

export interface AdminMenuGroup {
  expanded: any;
  selected: any;
  active: any;
  group: string;
  separator?: boolean;
  items: AdminMenuItem[];
}
