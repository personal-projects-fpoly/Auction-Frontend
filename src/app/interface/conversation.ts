import { IAuth } from './auth';
import { IMessage } from './message';

export interface IConversation {
  _id?: string;
  userId: any;
  messages: IMessage[];
  star: boolean;
  label?: string;
  status?: string;
  category?: string;
}
