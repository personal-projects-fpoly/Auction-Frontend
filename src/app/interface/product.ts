import { IBid } from './bid';

export interface IProduct {
  _id?: string;
  name: string;
  shortDescription?: string;
  description?: string;
  category: string;
  thumbnail: string;
  images?: string[];
  startPrice: number;
  currentPrice: number;
  startTime: Date;
  endTime: Date;
  step: number;
  bids: IBid[];
}
