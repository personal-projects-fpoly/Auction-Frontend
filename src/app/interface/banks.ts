export interface IBank {
  id: number;
  name: string;
  code: string;
  bin: string;
  shortName: string;
  logo: string;
  transferSupported: number;
  lookupSupported: number;
  short_name: string;
  support: number;
  isTransfer: number;
  swift_code: string;
}

export interface IQrResponse {
  code: string;
  desc: string;
  data: IQrCode;
}

export interface IQrCode {
  qrCode: string;
  qrDataURL: string;
}

export interface IBankList {
  code: string;
  desc: string;
  data: IBank[];
}

export interface IDataBank {
  message: string;
  data: IBankList;
}
