export interface ClientMenuItem {
  label: string;
  url?: string;
  items?: {
    label: string;
    url?: string;
  }[];
  command?: () => void;
}
