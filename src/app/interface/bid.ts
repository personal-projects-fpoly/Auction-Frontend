export interface IBid {
  _id?: string;
  user: string;
  amount: number;
  bidTime: Date;
}
