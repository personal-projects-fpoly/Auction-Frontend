interface IPayment {
  _id: string;
  user: string;
  amount: number;
  status: string;
  time: Date;
}

interface ICreatePayment {
  userId: string;
  addInfo: string;
  startTime: number;
  paymentMethod: string;
  amount: number;
}
export { IPayment, ICreatePayment };
