export interface IMessage {
  sender: {
    senderId: string;
    name: string;
    avatar: string;
  };
  content: string | null | undefined;
  status: string;
  timestamp: Date;
}
