import { IProduct } from './product';

export interface ISection {
  titleSection: string;
  titleCol: string;
  products: IProduct[];
}
