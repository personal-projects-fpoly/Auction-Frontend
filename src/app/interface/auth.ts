export interface IAuth {
  subscribe(arg0: (user: IAuth) => void): unknown;
  _id: string;
  username: string;
  password: string;
  name: string;
  role: string;
  email: string;
  balance: number;
}
