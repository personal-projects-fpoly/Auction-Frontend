export interface MenuAdminItem {
  label: string;
  route: string;
  icon?: string;
  active: boolean;
  expanded: boolean;
  children?: any;
}
