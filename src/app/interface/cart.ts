import { IProduct } from './product';

export interface ICart {
  _id: string;
  user: string;
  products: ICartItem[];
}

export interface ICartItem {
  _id: string;
  productId: IProduct;
  quantity: number;
  price: number;
}
