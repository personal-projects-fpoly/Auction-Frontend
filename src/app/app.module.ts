import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { ContextMenuModule } from 'primeng/contextmenu';
import { MenubarModule } from 'primeng/menubar';
import { GoogleMapsModule } from '@angular/google-maps';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { AvatarModule } from 'primeng/avatar';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { SidebarModule } from 'primeng/sidebar';
import { AnimateOnScrollModule } from 'primeng/animateonscroll';
import { StepperModule } from 'primeng/stepper';
import { SplitterModule } from 'primeng/splitter';
import { CardModule } from 'primeng/card';
import { MenuModule } from 'primeng/menu';
import { BadgeModule } from 'primeng/badge';
import { RippleModule } from 'primeng/ripple';
import { StyleClassModule } from 'primeng/styleclass';
import { TabViewModule } from 'primeng/tabview';
import { MessageService } from 'primeng/api';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { FocusTrapModule } from 'primeng/focustrap';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { PanelMenuModule } from 'primeng/panelmenu';
import { InputGroupModule } from 'primeng/inputgroup';
import { InputGroupAddonModule } from 'primeng/inputgroupaddon';

import { MenuAdminService } from './service/menu-admin.service';
import { AppJsonService } from './service/menu/app-json.service';

import {
  BadgeCheck,
  ChevronDown,
  ChevronsLeft,
  CircleArrowLeft,
  CircleArrowRight,
  Gavel,
  Heart,
  Image,
  Layers3,
  LayoutDashboard,
  LucideAngularModule,
  Mail,
  MailQuestion,
  MailWarning,
  MapPin,
  MessageCircleMore,
  MessageSquareText,
  PackageSearch,
  Pencil,
  PhoneCall,
  PieChart,
  Search,
  SendHorizontal,
  Share2,
  Star,
  Sword,
  Trash2,
  Users,
  X,
  ChevronRight,
  ChevronLeft,
  Send,
  BadgeCheckIcon,
  MessageSquareMore,
} from 'lucide-angular';

import { ClientHomeComponent } from './pages/client/home/home.component';
import { ClientHeaderComponent } from './pages/client/components/header/header.component';
import { ClientFooterComponent } from './pages/client/components/footer/footer.component';
import { ClientComponent } from './pages/client/client.component';
import { ClientNewsComponent } from './pages/client/news/news.component';
import { ClientCartComponent } from './pages/client/components/cart/cart.component';
import { ClientCheckoutComponent } from './pages/client/checkout/checkout.component';
import { ClientAccountComponent } from './pages/client/account/account.component';
import { ClientDepositComponent } from './pages/client/account/components/deposit/deposit.component';
import { ClientAccountMenuComponent } from './pages/client/account/components/account-menu/account-menu.component';
import { ClientAccountInfomationComponent } from './pages/client/account/components/account-infomation/account-infomation.component';
import { ClientNavItemComponent } from './pages/client/components/header/components/nav-item/nav-item.component';
import { ClientItemComponent } from './pages/client/components/header/components/item/item.component';
import { ContainerComponent } from './pages/client/components/container/container.component';
import { ClientBannerComponent } from './pages/client/home/components/banner/banner.component';
import { ClientSectionHomeComponent } from './pages/client/home/components/section-home/section-home.component';
import { ClientProductItemComponent } from './pages/client/components/product-item/product-item.component';
import { ClientSponsorComponent } from './pages/client/home/components/sponsor/sponsor.component';
import { ClientSectionNotificationComponent } from './pages/client/home/components/section-notification/section-notification.component';
import { ClientNotificationItemComponent } from './pages/client/components/notification-item/notification-item.component';
import { ClientSigninComponent } from './pages/client/signin/signin.component';
import { ClientSignupComponent } from './pages/client/signup/signup.component';
import { ClientDetailComponent } from './pages/client/detail/detail.component';
import { ClientContactComponent } from './pages/client/contact/contact.component';
import { ClientAboutComponent } from './pages/client/about/about.component';
import { ClientListComponent } from './pages/client/list/list.component';
import { ClientSidebarComponent } from './pages/client/list/components/sidebar/sidebar.component';
import { ClientMainListComponent } from './pages/client/list/components/main-list/main-list.component';
import { ClientPopupComponent } from './pages/client/detail/components/popup/popup.component';
import { ClientDescriptionComponent } from './pages/client/detail/components/description/description.component';
import { ClientBidsAuctionComponent } from './pages/client/detail/components/bids-auction/bids-auction.component';
import { ClientChatBoxComponent } from './pages/client/components/chat-box/chat-box.component';

import { AdminComponent } from './pages/admin/admin.component';
import { AdminDashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { AdminFooterComponent } from './pages/admin/components/footer-admin/footer-admin.component';
import { AdminSidebarComponent } from './pages/admin/components/sidebar-admin/sidebar-admin.component';
import { AdminNavbarComponent } from './pages/admin/components/navbar-admin/navbar-admin.component';
import { AdminNavbarMenuComponent } from './pages/admin/components/navbar-admin/navbar-menu/navbar-menu.component';
import { AdminNavbarMobileComponent } from './pages/admin/components/navbar-admin/navbar-mobile/navbar-mobilecomponent';
import { AdminNavbarMobileMenuComponent } from './pages/admin/components/navbar-admin/navbar-mobile/navbar-mobile-menu/navbar-mobile-menu.component';
import { AdminNavbarMobileSubmenuComponent } from './pages/admin/components/navbar-admin/navbar-mobile/navbar-mobile-submenu/navbar-mobile-submenu.component';
import { AdminNavbarSubmenuComponent } from './pages/admin/components/navbar-admin/navbar-submenu/navbar-submenu.component';
import { AdminProfileMenuComponent } from './pages/admin/components/navbar-admin/profile-menu/profile-menu.component';
import { AdminResponsiveHelperComponent } from './pages/admin/responsive-helper/responsive-helper.component';
import { AdminProductComponent } from './pages/admin/product/product.component';
import { AdminProductAddComponent } from './pages/admin/product/components/product-add/product-add.component';
import { AdminProductEditComponent } from './pages/admin/product/components/product-edit/product-edit.component';
import { AdminProductListComponent } from './pages/admin/product/components/product-list/product-list.component';
import { AdminCategoryComponent } from './pages/admin/category/category.component';
import { AdminCategoryAddComponent } from './pages/admin/category/category-add/category-add.component';
import { AdminCategoryListComponent } from './pages/admin/category/category-list/category-list.component';
import { AdminCategoryEditComponent } from './pages/admin/category/category-edit/category-edit.component';
import { AdminProductCreateComponent } from './pages/admin/product/components/product-create/product-create.component';
import { AdminConversationComponent } from './pages/admin/conversation/conversation.component';
import { AdminMessageMenuComponent } from './pages/admin/conversation/components/message-menu/message-menu.component';
import { AdminMessageListComponent } from './pages/admin/conversation/components/message-list/message-list.component';
import { AdminMessageTextingComponent } from './pages/admin/conversation/components/message-texting/message-texting.component';
import { AdminMessageTopbarComponent } from './pages/admin/conversation/components/message-topbar/message-topbar.component';
import { AdminMessageLabelComponent } from './pages/admin/conversation/message-label/message-label.component';
import { AdminMessageStatusComponent } from './pages/admin/conversation/message-status/message-status.component';
import { AdminMessageStarComponent } from './pages/admin/conversation/message-star/message-star.component';
import { AdminMessagesComponent } from './pages/admin/conversation/messages/messages.component';
import { ClientHeaderMobileMenuComponent } from './pages/client/components/header/components/client-header-mobile-menu/client-header-mobile-menu.component';
import { CustomButtonComponent } from './components/ui/custom-button/custom-button.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientHomeComponent,
    ClientHeaderComponent,
    ClientFooterComponent,
    ClientComponent,
    AdminComponent,
    ClientNavItemComponent,
    ClientItemComponent,
    ContainerComponent,
    ClientBannerComponent,
    ClientSectionHomeComponent,
    ClientProductItemComponent,
    ClientSponsorComponent,
    ClientSectionNotificationComponent,
    ClientNotificationItemComponent,
    AdminDashboardComponent,
    AdminFooterComponent,
    AdminSidebarComponent,
    AdminNavbarComponent,
    AdminFooterComponent,
    AdminSidebarComponent,
    AdminNavbarComponent,
    AdminNavbarMenuComponent,
    AdminNavbarMobileComponent,
    AdminNavbarMobileMenuComponent,
    AdminNavbarMobileSubmenuComponent,
    AdminNavbarSubmenuComponent,
    AdminProfileMenuComponent,
    AdminResponsiveHelperComponent,
    AdminProductComponent,
    AdminProductAddComponent,
    AdminProductEditComponent,
    AdminProductListComponent,
    AdminCategoryComponent,
    AdminCategoryAddComponent,
    AdminCategoryListComponent,
    AdminCategoryEditComponent,
    AdminProductCreateComponent,
    ClientSigninComponent,
    ClientSignupComponent,
    ClientDetailComponent,
    ClientContactComponent,
    ClientAboutComponent,
    ClientListComponent,
    ClientSidebarComponent,
    ClientMainListComponent,
    ClientPopupComponent,
    ClientDescriptionComponent,
    ClientBidsAuctionComponent,
    ClientChatBoxComponent,
    AdminConversationComponent,
    AdminMessageMenuComponent,
    AdminMessageListComponent,
    AdminMessageTextingComponent,
    AdminMessageTopbarComponent,
    AdminMessageLabelComponent,
    AdminMessageStatusComponent,
    AdminMessageStarComponent,
    AdminMessagesComponent,
    ClientNewsComponent,
    ClientCartComponent,
    ClientCheckoutComponent,
    ClientAccountComponent,
    ClientDepositComponent,
    ClientAccountMenuComponent,
    ClientAccountInfomationComponent,
    ClientHeaderMobileMenuComponent,
    CustomButtonComponent,
  ],
  providers: [MenuAdminService, AppJsonService, DatePipe, MessageService],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularEditorModule,
    GoogleMapsModule,
    ButtonModule,
    ToastModule,
    InputTextModule,
    DialogModule,
    AvatarModule,
    PanelModule,
    TieredMenuModule,
    SidebarModule,
    ContextMenuModule,
    AnimateOnScrollModule,
    StepperModule,
    DropdownModule,
    FocusTrapModule,
    SplitterModule,
    CardModule,
    MenuModule,
    BadgeModule,
    RippleModule,
    StyleClassModule,
    TabViewModule,
    ProgressSpinnerModule,
    PanelMenuModule,
    InputGroupModule,
    InputGroupAddonModule,
    CommonModule,

    LucideAngularModule.pick({
      ChevronDown,
      Search,
      Sword,
      LayoutDashboard,
      ChevronsLeft,
      PackageSearch,
      PieChart,
      Layers3,
      X,
      BadgeCheck,
      CircleArrowRight,
      CircleArrowLeft,
      Gavel,
      Share2,
      PhoneCall,
      Mail,
      MapPin,
      MessageCircleMore,
      Image,
      Heart,
      SendHorizontal,
      MessageSquareText,
      Star,
      Users,
      Pencil,
      MailWarning,
      MailQuestion,
      Trash2,
      ChevronRight,
      ChevronLeft,
      Send,
      BadgeCheckIcon,
      MessageSquareMore,
    }),
    BrowserAnimationsModule,
    MenubarModule,
    TableModule,
    PaginatorModule,
    MenubarModule,
    NgbModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
