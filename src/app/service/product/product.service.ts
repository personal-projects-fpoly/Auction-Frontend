import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  API = environment.API_URL + '/product/';
  getProducts(): Observable<any> {
    return this.http.get(this.API);
  }

  getProduct(id: string): Observable<any> {
    return this.http.get(this.API + id);
  }

  getAuctioned(): Observable<any> {
    return this.http.get(this.API + 'ed');
  }
  getAuctioning(): Observable<any> {
    return this.http.get(this.API + 'ing');
  }
  getWillAuction(): Observable<any> {
    return this.http.get(this.API + 'will');
  }
  addProduct(data: any): Observable<any> {
    return this.http.post(this.API, data);
  }
  auctionProduct(id: string, data: any): Observable<any> {
    return this.http.put(this.API + 'auction/' + id, data);
  }
  updateProduct(id: string, data: any): Observable<any> {
    return this.http.put(this.API + id, data);
  }

  deleteProduct(id: string): Observable<any> {
    return this.http.delete(this.API + id);
  }
  getOngoingAuctions(): Observable<any> {
    return this.http.get(`${this.API}ongoing`);
  }

  getTotalBids(): Observable<any> {
    return this.http.get(`${this.API}total`);
  }
}
