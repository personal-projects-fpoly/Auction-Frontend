import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMessage } from '../../interface/message';
import { IConversation } from '../../interface/conversation';
import { environment } from 'environment';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  constructor(private http: HttpClient) {}

  URL_API = environment.API_URL + '/message';
  getAll(): Observable<any> {
    return this.http.get(this.URL_API);
  }

  getById(id: string): Observable<any> {
    return this.http.get(this.URL_API + '/' + id);
  }

  getByUser(id?: string): Observable<any> {
    return this.http.get(this.URL_API + '/user/' + id);
  }
  getByCategory(id?: string): Observable<any> {
    return this.http.get(this.URL_API + '/category/' + id);
  }
  getByStatus(id?: string): Observable<any> {
    return this.http.get(this.URL_API + '/status/' + id);
  }
  getByStar(): Observable<any> {
    return this.http.get(this.URL_API + '/star');
  }
  getByLabel(id: string): Observable<any> {
    return this.http.get(this.URL_API + '/label/' + id);
  }
  add(data: any): Observable<any> {
    return this.http.post(this.URL_API, data);
  }
  update(id: string, data: IMessage): Observable<any> {
    return this.http.put(this.URL_API + '/' + id, data);
  }
  delete(id: string): Observable<any> {
    return this.http.delete(this.URL_API + '/' + id);
  }
}
