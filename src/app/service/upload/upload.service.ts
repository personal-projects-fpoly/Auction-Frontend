import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environment';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  constructor(private http: HttpClient) {}

  API_URL = environment.API_URL + '/upload';

  upload(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post(this.API_URL, formData);
  }

  getFiles(url: string) {
    return this.http.get(this.API_URL + '/images/' + url);
  }
}
