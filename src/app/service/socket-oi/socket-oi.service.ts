// socket-oi.service.ts

import { Injectable } from '@angular/core';
import { environment } from 'environment';
import { io, Socket } from 'socket.io-client';

@Injectable({
  providedIn: 'root',
})
export class SocketOiService {
  private socket: Socket;

  constructor() {
    this.socket = io(environment.API_URL);
  }
  getSocket(): Socket {
    return this.socket;
  }
  disconnect(): void {
    this.socket.disconnect();
  }
}
