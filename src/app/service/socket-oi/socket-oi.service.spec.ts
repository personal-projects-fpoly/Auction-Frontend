import { TestBed } from '@angular/core/testing';

import { SocketOiService } from './socket-oi.service';

describe('SocketOiService', () => {
  let service: SocketOiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SocketOiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
