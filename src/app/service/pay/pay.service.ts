import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environment';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PayService {
  private URL_API = environment.API_URL + '/payment';

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get(this.URL_API);
  }

  getBanks(): Observable<any> {
    return this.http.get(this.URL_API + '/banks');
  }

  getOne(id: string): Observable<any> {
    return this.http.get(this.URL_API + '/' + id);
  }

  create(data: any): any {
    return this.http.post(this.URL_API, data);
  }

  createQrCode(data: any): any {
    return this.http.post(this.URL_API + '/qr', data);
  }
}
