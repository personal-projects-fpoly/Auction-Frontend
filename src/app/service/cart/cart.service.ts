import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  constructor(private http: HttpClient) {}

  URL_API = environment.API_URL + '/cart';
  getAll(): Observable<any> {
    return this.http.get(this.URL_API);
  }

  getOne(id: string) {
    return this.http.get(this.URL_API + '/' + id);
  }

  add(data: any) {
    return this.http.post(this.URL_API, data);
  }

  update(id: string, data: any) {
    return this.http.put(this.URL_API + '/' + id, data);
  }

  delete(id: string) {
    return this.http.delete(this.URL_API + '/' + id);
  }
}
