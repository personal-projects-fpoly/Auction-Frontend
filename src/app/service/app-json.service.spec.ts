import { TestBed } from '@angular/core/testing';

import { AppJsonService } from './menu/app-json.service';

describe('AppJsonService', () => {
  let service: AppJsonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppJsonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
