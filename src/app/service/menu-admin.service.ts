import { Injectable } from '@angular/core';
import pagesMenu from 'src/assets/data/admin/sidebar';
@Injectable({
  providedIn: 'root',
})
export class MenuAdminService {
  showSideBar = true;
  pagesMenu = pagesMenu;
}
