import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environment';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { IAuth } from 'src/app/interface/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private userSubject: BehaviorSubject<IAuth | null>;
  private API_URL = environment.API_URL + '/auth';

  constructor(private http: HttpClient) {
    this.userSubject = new BehaviorSubject<IAuth | null>(null);
  }

  public get userValue(): Observable<IAuth | null> {
    return this.userSubject.asObservable();
  }

  public setValue(newUser: IAuth | null): void {
    this.userSubject.next(newUser);
  }

  signin(data: any): Observable<any> {
    return this.http
      .post(`${this.API_URL}/signin`, data, { withCredentials: true })
      .pipe(
        tap((response: any) => {
          if (response) {
            sessionStorage.setItem('user', response.data);
            this.setValue(response.data);
          }
        }),
        catchError((error) => {
          console.error('Error during signin:', error);
          return throwError(() => new Error('Signin failed'));
        })
      );
  }

  signup(data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/signup`, data).pipe(
      tap((response) => {
        console.log('Signup successful:', response);
      }),
      catchError((error) => {
        console.error('Error during signup:', error);
        return throwError(() => new Error('Signup failed'));
      })
    );
  }
  signout(): void {
    this.http
      .post(`${this.API_URL}/signout`, {}, { withCredentials: true }) // Đặt { withCredentials: true } ở đây
      .pipe(
        tap(() => {
          sessionStorage.removeItem('user');
          this.setValue(null);
        }),
        catchError((error) => {
          console.error('Error during sign out:', error);
          return throwError(() => new Error('Signout failed'));
        })
      )
      .subscribe(
        () => {
          console.log('Successfully signed out.');
        },
        (error) => {
          console.error('Error during sign out:', error);
        }
      );
  }

  protectedAuth(): Observable<any> {
    return this.http
      .get(`${this.API_URL}/protected`, { withCredentials: true })
      .pipe(
        tap((response: any) => {
          if (response) {
            sessionStorage.setItem('user', response.data);
            this.setValue(response.data);
          }
        }),
        catchError((error) => {
          console.error('Error during protectedAuth:', error);
          return throwError(() => new Error('Protected route access failed'));
        })
      );
  }
}
