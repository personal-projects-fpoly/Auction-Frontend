import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientNewsComponent } from './news.component';

describe('ClientNewsComponent', () => {
  let component: ClientNewsComponent;
  let fixture: ComponentFixture<ClientNewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientNewsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
