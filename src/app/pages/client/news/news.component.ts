import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrl: './news.component.scss',
})
export class ClientNewsComponent {
  items: { label?: string; icon?: string; separator?: boolean }[] = [];
  itemNav: any = [];

  constructor(private router: Router) {}

  ngOnInit() {
    this.itemNav = [
      {
        label: 'Thông báo',
        icon: 'pi pi-palette',
        items: [
          {
            label: 'Theming',
            route: '/theming',
          },
          {
            label: 'Colors',
            route: '/colors',
          },
        ],
      },
      {
        label: 'Thông báo đấu giá',
        icon: 'pi pi-link',
        command: () => {
          this.router.navigate(['/installation']);
        },
      },
      {
        label: 'Tin khác',
        icon: 'pi pi-home',
        items: [
          {
            label: 'Angular',
            url: 'https://angular.dev/',
          },
          {
            label: 'Vite.js',
            url: 'https://vitejs.dev/',
          },
        ],
      },
    ];
    this.items = [
      {
        label: 'Refresh',
        icon: 'pi pi-refresh',
      },
      {
        label: 'Search',
        icon: 'pi pi-search',
      },
      {
        separator: true,
      },
      {
        label: 'Delete',
        icon: 'pi pi-times',
      },
    ];
  }
  visible = false;

  toggleVisibility() {
    this.visible = !this.visible;
  }
}
