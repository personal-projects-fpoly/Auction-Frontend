import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss',
})
export class ClientContactComponent {
  center: google.maps.LatLngLiteral = { lat: 21.028511, lng: 105.804817 };
  zoom = 15;
}
