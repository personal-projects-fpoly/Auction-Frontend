import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../service/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class ClientSignupComponent {
  constructor(private authService: AuthService, private route: Router) {}

  formSignup = new FormGroup({
    accountType: new FormControl('individual'),
    lastName: new FormControl('Nguyễn', Validators.required),
    middleName: new FormControl('Đình'),
    firstName: new FormControl('Tuân', Validators.required),
    username: new FormControl('', Validators.required),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    confirmPassword: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('088888888', Validators.required),
    gender: new FormControl('Nam'),
    birthDay: new FormControl('01'),
    birthMonth: new FormControl('01'),
    birthYear: new FormControl('2004'),
    province: new FormControl('Ho Chi Minh'),
    district: new FormControl('Quan 1'),
    ward: new FormControl('Phuong 1'),
    address: new FormControl('123 Pham Van Dong, Quan 1'),
    idCard: new FormControl('1234567890'),
    issueDate: new FormControl('01/01/2020'),
    issuePlace: new FormControl('TP Ho Chi Minh'),
    frontIdCard: new FormControl('image_front.jpg'),
    backIdCard: new FormControl('image_back.jpg'),
    bankAccount: new FormControl('1234567890'),
    bankName: new FormControl('Ngan hang ABC'),
    bankBranch: new FormControl('Chi nhanh XYZ'),
    accountHolder: new FormControl('Nguyen A'),
    terms: new FormControl(true, Validators.requiredTrue),
  });

  onSubmit() {
    console.log(this.formSignup.value);
    if (
      this.formSignup.value.password ===
        this.formSignup.value.confirmPassword &&
      this.formSignup.valid
    ) {
      this.authService
        .signup({
          name:
            this.formSignup.value.firstName +
            ' ' +
            this.formSignup.value.middleName +
            ' ' +
            this.formSignup.value.lastName,
          username: this.formSignup.value.username,
          email: this.formSignup.value.email,
          password: this.formSignup.value.password,
        })
        .subscribe(
          (res) => {
            alert('Signup successfully');
            this.formSignup.reset();
          },
          (error) => {
            console.log('Signup failed' + error);
          }
        );
    } else {
      alert('Invalid form');
    }
  }
}
