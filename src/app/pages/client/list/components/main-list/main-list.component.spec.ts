import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientMainListComponent } from './main-list.component';

describe('ClientMainListComponent', () => {
  let component: ClientMainListComponent;
  let fixture: ComponentFixture<ClientMainListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientMainListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientMainListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
