import { Component, Input } from '@angular/core';
import { IProduct } from '../../../../../interface/product';

@Component({
  selector: 'app-main-list',
  templateUrl: './main-list.component.html',
  styleUrl: './main-list.component.scss',
})
export class ClientMainListComponent {
  @Input() products: IProduct[] = [];
}
