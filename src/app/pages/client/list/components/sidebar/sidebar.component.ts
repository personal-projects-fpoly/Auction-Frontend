import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IProduct } from '../../../../../interface/product';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class ClientSidebarComponent {
  @Input() products: IProduct[] = [];
  @Output() setProducts = new EventEmitter<IProduct[]>();
  filteredProducts: IProduct[] = [];
  searchKeyword: string = '';
  fromDate: string = '';
  toDate: string = '';
  selectedStatus: string = '';
  selectedAuctionType: string = '';

  ngOnChanges() {
    this.filterProducts();
  }

  onInputChange(event: any): void {
    this.searchKeyword = event.target.value;
    this.filterProducts();
  }

  onDateChange(event: any): void {
    const { name, value } = event.target;
    if (name === 'fromDate') {
      this.fromDate = value;
    } else if (name === 'toDate') {
      this.toDate = value;
    }
    this.filterProducts();
  }

  onStatusChange(event: any, status: string): void {
    this.selectedStatus = status;
    this.filterProducts();
  }

  filterProducts() {
    this.filteredProducts = this.products.filter((product) => {
      const matchesKeyword = this.searchKeyword
        ? product.name.toLowerCase().includes(this.searchKeyword.toLowerCase())
        : true;
      const matchesFromDate = this.fromDate
        ? new Date(product.startTime) >= new Date(this.fromDate)
        : true;
      const matchesToDate = this.toDate
        ? new Date(product.endTime) <= new Date(this.toDate)
        : true;
      const matchesStatus =
        this.selectedStatus && this.selectedStatus !== 'all'
          ? this.getAuctionStatus(product) === this.selectedStatus
          : true;

      return (
        matchesKeyword && matchesFromDate && matchesToDate && matchesStatus
      );
    });

    this.setProducts.emit(this.filteredProducts);
    console.log(this.filteredProducts);
  }

  getAuctionStatus(product: IProduct): string {
    const now = new Date();
    if (new Date(product.startTime) > now) {
      return 'upcoming';
    } else if (new Date(product.endTime) < now) {
      return 'completed';
    } else {
      return 'ongoing';
    }
  }

  onSubmit(): void {
    console.log('Form submitted with:', {
      searchKeyword: this.searchKeyword,
      fromDate: this.fromDate,
      toDate: this.toDate,
      selectedStatus: this.selectedStatus,
      selectedAuctionType: this.selectedAuctionType,
    });
    console.log(this.filteredProducts);
  }
}
