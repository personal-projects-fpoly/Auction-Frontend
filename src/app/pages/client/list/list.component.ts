import { Component } from '@angular/core';
import { IProduct } from '../../../interface/product';
import { ProductService } from '../../../service/product/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss',
})
export class ClientListComponent {
  constructor(private productService: ProductService) {}
  products: IProduct[] = [];
  productFilter: IProduct[] = [];
  ngOnInit() {
    this.productService.getProducts().subscribe((data) => {
      this.products = data;
      this.productFilter = data;
    });
  }

  setProducts(products: IProduct[]) {
    this.products = products;
  }
}
