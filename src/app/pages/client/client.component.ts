import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrl: './client.component.scss',
})
export class ClientComponent {
  isScrolled = true;
  showSignin = false;
  data: any[] = [];
  sidebarVisible: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scrollPosition =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;
    this.isScrolled = !(scrollPosition > 50);
  }
  toggleSidebar() {
    this.sidebarVisible = true;
    console.log('ok');
  }
  toggleSignin() {
    this.showSignin = !this.showSignin;
    console.log(this.showSignin);
  }
}
