import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from './../../../service/product/product.service';
import { IProduct } from '../../../interface/product';
import { interval, Subscription } from 'rxjs';
import { SocketOiService } from '../../../service/socket-oi/socket-oi.service';
import { AuthService } from 'src/app/service/auth/auth.service';
import { IAuth } from 'src/app/interface/auth';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class ClientDetailComponent implements OnInit, OnDestroy {
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private socketService: SocketOiService,
    private authService: AuthService
  ) {
    this.socketService.getSocket().on('updateProduct', (data: any) => {
      console.log('Received bid update:', data);
      if (data._id === this.product._id) {
        this.product = data;
      }
    });
  }

  imageMain: string = '';
  user: IAuth | null = null;
  product: IProduct = {} as IProduct;
  productId: string = this.route.snapshot.params['id'];
  auctionStarted: boolean = false;
  auctionEnded: boolean = false;
  startTimer: any = {};
  endTimer: any = {};
  showAuction: boolean = false;
  minPrice: number = 1;
  info: string = 'bids';
  private timerSubscription: Subscription | undefined;

  toggleAuction() {
    if (this.showAuction) {
      this.showAuction = !this.showAuction;
    }
    if (!this.user) {
      alert('Please login to continue');
      return;
    } else {
      this.showAuction = !this.showAuction;
    }
  }

  ngOnInit() {
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
    this.productService.getProduct(this.productId).subscribe((data) => {
      this.product = data;
      this.imageMain = data.thumbnail;
      this.minPrice = data.currentPrice + data.step;
      const currentTime = new Date().getTime();

      if (
        currentTime >= new Date(data.startTime).getTime() &&
        currentTime <= new Date(data.endTime).getTime()
      ) {
        this.auctionStarted = true;
        this.endTimer = this.getTimeRemaining(
          new Date(data.endTime).getTime() - currentTime
        );
        this.timerSubscription = interval(1000).subscribe(() => {
          const timeLeft =
            new Date(data.endTime).getTime() - new Date().getTime();
          this.endTimer = this.getTimeRemaining(timeLeft);
        });
      } else if (currentTime > new Date(data.endTime).getTime()) {
        this.auctionEnded = true;
      } else {
        this.startTimer = this.getTimeRemaining(
          new Date(data.startTime).getTime() - currentTime
        );
        this.timerSubscription = interval(1000).subscribe(() => {
          const timeLeft =
            new Date(data.startTime).getTime() - new Date().getTime();
          this.startTimer = this.getTimeRemaining(timeLeft);
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  getTimeRemaining(total: number) {
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
    const days = Math.floor(total / (1000 * 60 * 60 * 24));
    return {
      total,
      days,
      hours,
      minutes,
      seconds,
    };
  }

  changeImage(image: string) {
    this.imageMain = image;
  }
  changInfomation(info: string) {
    this.info = info;
  }
}
