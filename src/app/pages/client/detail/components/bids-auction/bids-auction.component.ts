import { Component, Input } from '@angular/core';
import { IProduct } from '../../../../../interface/product';
import { IBid } from '../../../../../interface/bid';

@Component({
  selector: 'app-bids-auction',
  templateUrl: './bids-auction.component.html',
  styleUrl: './bids-auction.component.scss',
})
export class ClientBidsAuctionComponent {
  @Input() bids: IBid[] = {} as IBid[];
  first = 0;
  rows = 10;

  onPageChange(event: any) {
    this.first = event.first;
    this.rows = event.rows;
  }
}
