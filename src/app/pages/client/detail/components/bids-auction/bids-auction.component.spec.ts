import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientBidsAuctionComponent } from './bids-auction.component';

describe('ClientBidsAuctionComponent', () => {
  let component: ClientBidsAuctionComponent;
  let fixture: ComponentFixture<ClientBidsAuctionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientBidsAuctionComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientBidsAuctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
