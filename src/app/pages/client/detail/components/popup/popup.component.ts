import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductService } from '../../../../../service/product/product.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IProduct } from '../../../../../interface/product';
import { AuthService } from 'src/app/service/auth/auth.service';
import { IAuth } from 'src/app/interface/auth';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
})
export class ClientPopupComponent {
  @Output() toggleAuction = new EventEmitter();
  @Input() product: IProduct = {} as IProduct;
  @Input() minPrice: number = 1;
  submitted: boolean = false;
  formAuction: FormGroup | undefined;
  user: IAuth | null = null;

  constructor(
    private Auction: ProductService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.formAuction = new FormGroup({
      price: new FormControl(this.minPrice, [
        Validators.required,
        Validators.min(this.minPrice),
      ]),
    });
  }

  offAuction() {
    this.toggleAuction.emit();
  }

  show() {}
  onSubmit() {
    if (!this.formAuction) return;
    this.submitted = true;
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
    if (!this.user) {
      alert('Please login to continue');
      return;
    }
    if (this.formAuction.valid && this.product._id) {
      const data = {
        currentPrice: this.formAuction.value.price,
        bids: [
          {
            user: this.user._id,
            amount: this.formAuction.value.price,
            bidTime: new Date(),
          },
        ],
      };
      this.Auction.auctionProduct(this.product._id, data).subscribe(
        (res: any) => {
          console.log(res);
          this.show();
          alert('Auction successfully');
          this.toggleAuction.emit();
          this.formAuction?.reset();
        },
        (e) => {
          console.log(e);
          alert(e.error.message);
          console.log(this.product.currentPrice);
        }
      );
    } else {
      alert('Error');
    }
  }
}
