import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientNotificationItemComponent } from './notification-item.component';

describe('ClientNotificationItemComponent', () => {
  let component: ClientNotificationItemComponent;
  let fixture: ComponentFixture<ClientNotificationItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientNotificationItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientNotificationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
