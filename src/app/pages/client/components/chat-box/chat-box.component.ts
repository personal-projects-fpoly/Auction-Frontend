import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SocketOiService } from '../../../../service/socket-oi/socket-oi.service';
import { MessageService } from '../../../../service/message/message.service';
import { IMessage } from '../../../../interface/message';
import { IConversation } from '../../../../interface/conversation';
import { IAuth } from 'src/app/interface/auth';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.scss'],
})
export class ClientChatBoxComponent {
  @ViewChild('scrollMessage') chatContainer: ElementRef | undefined;
  @Output() toggleSignin = new EventEmitter<boolean>();

  user: IAuth | null = null;
  conversation: IConversation = {} as IConversation;
  showBox: boolean = true;
  message: string = '';
  texting: boolean = false;
  isLandscape: boolean = false;
  formMessage = new FormGroup({
    message: new FormControl('', Validators.required),
  });

  constructor(
    private socketService: SocketOiService,
    private messageService: MessageService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.checkOrientation();
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
    console.log(this.user);

    if (this.user) {
      this.messageService.getByUser(this.user._id).subscribe(
        (data: IConversation) => {
          this.conversation = data;
          if (this.conversation._id) {
            this.socketService
              .getSocket()
              .on(this.conversation._id, (data: IMessage) => {
                console.log('Message update:', data);
                this.conversation.messages.push(data);
                this.scrollToBottom();
              });
          }
        },
        () => {
          if (this.user) {
            this.messageService
              .add(this.user._id)
              .subscribe((data: IConversation) => {
                this.conversation = data;
              });
          }
        }
      );
    }
  }
  checkOrientation(): void {
    const isMobile = window.matchMedia('(max-width: 767px)').matches;
    const check = window.matchMedia('(orientation: landscape)').matches;

    this.isLandscape = isMobile && check;
  }
  @HostListener('window:resize', ['$event'])
  onResize(event: Event): void {
    this.checkOrientation();
  }
  toggleBox() {
    if (!this.user) {
      this.toggleSignin.emit();
      return;
    }
    this.showBox = !this.showBox;
  }
  handleMessage(event: any) {
    this.message = event.target.value;
    if (this.message) {
      this.texting = true;
    } else {
      this.texting = false;
    }
  }
  sendMessage() {
    if (this.formMessage.valid && this.user) {
      const newMessage: any = {
        sender: {
          senderId: this.user._id,
          name: this.user.name,
          avatar: 'https://picsum.photos/200/300',
        },
        content: this.formMessage.value.message,
        status: 'sent',
      };

      this.messageService
        .update(this.user._id, newMessage)
        .subscribe((data: IMessage) => {
          console.log('Update success');
          this.formMessage.reset();
          this.texting = false;
          this.scrollToBottom();
        });
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }
  private scrollToBottom(): void {
    if (this.chatContainer) {
      const container = this.chatContainer.nativeElement;
      container.scrollTop = container.scrollHeight;
    }
  }
}
