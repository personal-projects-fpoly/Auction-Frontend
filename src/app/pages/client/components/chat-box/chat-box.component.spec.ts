import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientChatBoxComponent } from './chat-box.component';

describe('ClientChatBoxComponent', () => {
  let component: ClientChatBoxComponent;
  let fixture: ComponentFixture<ClientChatBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientChatBoxComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientChatBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
