import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientProductItemComponent } from './product-item.component';

describe('ClientProductItemComponent', () => {
  let component: ClientProductItemComponent;
  let fixture: ComponentFixture<ClientProductItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientProductItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientProductItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
