import { Component, Input } from '@angular/core';
import { IProduct } from '../../../../interface/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrl: './product-item.component.scss',
})
export class ClientProductItemComponent {
  @Input() product: IProduct = {} as IProduct;
  @Input() title: string = '';
  formatDate(dateString: Date): string {
    const date = new Date(dateString);
    return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
  }
  formatRemainingTime(endTime: Date): string {
    const remainingTimeInSeconds =
      (new Date(endTime).getTime() - Date.now()) / 1000;
    const hours = Math.floor(remainingTimeInSeconds / 3600);
    const minutes = Math.floor((remainingTimeInSeconds % 3600) / 60);
    const seconds = Math.floor(remainingTimeInSeconds % 60);
    return `${hours}h : ${minutes}p : ${seconds}s `;
  }
}
