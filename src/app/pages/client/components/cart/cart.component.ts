import { Router } from '@angular/router';
import { CartService } from './../../../../service/cart/cart.service';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { ContextMenu } from 'primeng/contextmenu';
import { IAuth } from 'src/app/interface/auth';
import { AuthService } from 'src/app/service/auth/auth.service';
import { ICart } from 'src/app/interface/cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.scss',
})
export class ClientCartComponent {
  @Output() toggleSidebar = new EventEmitter<boolean>();
  carts: ICart = {} as ICart;
  selectedId: any;
  user: IAuth | null = null;
  items: any[] = [];
  subtotal: number = 0;
  total: number = 0;
  discount: number = 0;

  @ViewChild('cm') cm: ContextMenu | undefined;

  constructor(
    private CartService: CartService,
    private Router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
    if (this.user) {
      this.CartService.getOne(this.user._id).subscribe(
        (data: any) => {
          this.carts = data;
          this.subtotal = data.products.reduce((acc: number, product: any) => {
            const price = parseFloat(product.price.$numberDecimal);
            return acc + price;
          }, 0);
          this.total = this.subtotal + this.discount;
          console.log(this.carts);
        },
        () => {
          console.log('error');
        }
      );
    }
  }

  onCheckout() {
    this.toggleSidebar.emit();
    console.log(this.carts);

    this.Router.navigate(['/checkout/' + this.carts._id]);
  }

  onContextMenu(event: any) {
    if (this.cm) {
      this.cm.target = event.currentTarget;
      this.cm.show(event);
    }
  }

  onHide() {
    this.selectedId = undefined;
  }
}
