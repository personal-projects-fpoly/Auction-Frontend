import { Router } from '@angular/router';
import { Component, EventEmitter, Output } from '@angular/core';
import { IAuth } from 'src/app/interface/auth';
import { AuthService } from 'src/app/service/auth/auth.service';
import Navs from 'src/assets/data/client/menuHeader';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
})
export class ClientHeaderComponent {
  @Output() toggleSignin = new EventEmitter<boolean>();
  @Output() toggleSidebar = new EventEmitter<boolean>();
  visible: boolean = false;
  login: boolean = false;
  user: IAuth | null = null;
  mobile: boolean = true;
  Navs = Navs;

  constructor(private Router: Router, private authService: AuthService) {}

  onMobile() {
    this.mobile = !this.mobile;
  }

  showDialog(show: boolean) {
    this.visible = show;
  }
  toggleMenu() {
    this.toggleSignin.emit();
  }

  onAccount() {
    this.visible = false;
    this.Router.navigate(['/account']);
  }

  logout() {
    this.authService.signout();
  }
  onCart() {
    this.toggleSidebar.emit();
    this.visible = false;
  }

  time: string = '';
  date: string = '';
  ngOnInit(): void {
    this.updateTime();
    setInterval(() => this.updateTime(), 1000);
    this.authService.userValue?.subscribe((data) => {
      if (data) {
        this.user = data;
        this.login = true;
      } else {
        this.user = null;
        this.login = false;
        this.showDialog(false);
      }
    });
  }

  updateTime(): void {
    const now = new Date();
    const daysOfWeek = [
      'Chủ Nhật',
      'Thứ Hai',
      'Thứ Ba',
      'Thứ Tư',
      'Thứ Năm',
      'Thứ Sáu',
      'Thứ Bảy',
    ];
    const dayOfWeek = daysOfWeek[now.getDay()];
    const day = String(now.getDate()).padStart(2, '0');
    const month = String(now.getMonth() + 1).padStart(2, '0');
    const year = now.getFullYear();
    const hours = String(now.getHours()).padStart(2, '0');
    const minutes = String(now.getMinutes()).padStart(2, '0');
    const seconds = String(now.getSeconds()).padStart(2, '0');
    this.time = `${hours}:${minutes}:${seconds}`;
    this.date = `${dayOfWeek}, ${day}/${month}/${year}`;
  }
}
