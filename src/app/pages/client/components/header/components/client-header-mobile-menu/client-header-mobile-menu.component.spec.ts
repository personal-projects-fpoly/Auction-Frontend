import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientHeaderMobileMenuComponent } from './client-header-mobile-menu.component';

describe('ClientHeaderMobileMenuComponent', () => {
  let component: ClientHeaderMobileMenuComponent;
  let fixture: ComponentFixture<ClientHeaderMobileMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientHeaderMobileMenuComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ClientHeaderMobileMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
