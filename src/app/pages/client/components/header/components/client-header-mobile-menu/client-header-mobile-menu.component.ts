import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-client-header-mobile-menu',
  templateUrl: './client-header-mobile-menu.component.html',
  styleUrl: './client-header-mobile-menu.component.scss',
})
export class ClientHeaderMobileMenuComponent {
  items: MenuItem[] = [];

  ngOnInit() {
    this.items = [
      {
        label: 'Tài Sản Đấu Giá',
        icon: 'pi pi-file',
        items: [
          {
            label: 'Tài Sản Nhà Nước',
            icon: 'pi pi-file',
            url: '/',
          },
          {
            label: 'Bất Động Sản',
            icon: 'pi pi-file',
            url: '/',
          },
          {
            label: 'Phương tiện - Xe Cộ',
            icon: 'pi pi-car',
            url: '/',
          },
          {
            label: 'Sưu tầm - nghệ thuật',
            icon: 'pi pi-palette',
            url: '/',
          },
          {
            label: 'Hàng hiệu xa xỉ',
            icon: 'pi pi-tag',
            url: '/',
          },
          {
            label: 'Tang vật tịch thu',
            icon: 'pi pi-box',
            url: '/',
          },
          {
            label: 'Tài sản khác',
            icon: 'pi pi-ellipsis-h',
            url: '/',
          },
        ],
      },
      {
        label: 'Cuộc đấu giá',
        icon: 'pi pi-gavel',
        items: [
          {
            label: 'Cuộc đấu giá sắp đấu giá',
            icon: 'pi pi-calendar',
            url: 'list',
          },
          {
            label: 'Cuộc đấu giá đang diễn ra',
            icon: 'pi pi-play',
            url: 'list',
          },
          {
            label: 'Cuộc đấu giá đã kết thúc',
            icon: 'pi pi-check',
            url: 'list',
          },
        ],
      },
      {
        label: 'Tin tức',
        icon: 'pi pi-info-circle',
        items: [
          {
            label: 'Thông báo',
            icon: 'pi pi-bell',
            url: 'news',
          },
          {
            label: 'Thông báo đấu giá',
            icon: 'pi pi-bullhorn',
            url: 'news',
          },
          {
            label: 'Tin khác',
            icon: 'pi pi-newspaper',
            url: 'news',
          },
        ],
      },
      {
        label: 'Giới Thiệu',
        icon: 'pi pi-info',
        url: 'about',
      },
      {
        label: 'Liên hệ',
        icon: 'pi pi-envelope',
        url: 'contact',
      },
    ];
  }
}
