import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientNavItemComponent } from './nav-item.component';

describe('ClientNavItemComponent', () => {
  let component: ClientNavItemComponent;
  let fixture: ComponentFixture<ClientNavItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientNavItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientNavItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
