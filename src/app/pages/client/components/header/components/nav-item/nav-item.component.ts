import { Component, Input } from '@angular/core';
import { ClientMenuItem } from 'src/app/interface/menuBar';
@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss'],
})
export class ClientNavItemComponent {
  @Input() nav!: ClientMenuItem;
  isMenuOpen = false;
  menuEffects = false;

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  onMouseEnter() {
    this.isMenuOpen = true;
    setTimeout(() => {
      this.menuEffects = true;
    }, 300);
  }

  onMouseLeave() {
    this.menuEffects = false;
    setTimeout(() => {
      this.isMenuOpen = false;
    }, 300);
  }
  trackByFn(index: number, item: any) {
    return item.id;
  }
}
