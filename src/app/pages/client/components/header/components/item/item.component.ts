import { Component, Input } from '@angular/core';
import { ClientMenuItem } from 'src/app/interface/menuBar';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrl: './item.component.scss',
})
export class ClientItemComponent {
  @Input() item!: ClientMenuItem;
}
