import { Component, EventEmitter, Output } from '@angular/core';
import { AuthService } from './../../../service/auth/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrl: './signin.component.scss',
})
export class ClientSigninComponent {
  @Output() toggleSignin = new EventEmitter();
  submitted: boolean = false;
  constructor(private AuthService: AuthService, private router: Router) {}

  formSignin = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  offSignin() {
    this.toggleSignin.emit();
  }

  onSubmit() {
    this.submitted = true;
    if (this.formSignin.valid) {
      this.AuthService.signin(this.formSignin.value).subscribe(
        (res: any) => {
          console.log(this.AuthService.userValue);
          if (res.data.role == 'admin') {
            this.router.navigate(['/admin']);
          } else {
          }
          this.toggleSignin.emit();
          alert('Signin successfully');
        },
        (err) => {
          alert(err.error.message);
          console.log(err);
        }
      );
    }
  }
}
