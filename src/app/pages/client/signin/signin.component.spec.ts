import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSigninComponent } from './signin.component';

describe('ClientSigninComponent', () => {
  let component: ClientSigninComponent;
  let fixture: ComponentFixture<ClientSigninComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientSigninComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientSigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
