import { PayService } from './../../../../../service/pay/pay.service';
import { Component, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ContextMenu } from 'primeng/contextmenu';
import { IAuth } from 'src/app/interface/auth';
import { ICreatePayment } from 'src/app/interface/payment';
import { AuthService } from 'src/app/service/auth/auth.service';
import { SocketOiService } from 'src/app/service/socket-oi/socket-oi.service';
@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrl: './deposit.component.scss',
})
export class ClientDepositComponent {
  paying: boolean = false;
  items: MenuItem[] | undefined;
  user: IAuth | null = null;
  paid: any = {};
  qrCode = '';
  amount: number = 0;
  timeLeft: any = {
    time: 0,
    s: 0,
    m: 0,
    h: 0,
  };
  startTimeEncoded: string = '';
  description: string = '';

  @ViewChild('cm') cm: ContextMenu | undefined;

  constructor(
    private PayService: PayService,
    private socketService: SocketOiService,
    private authService: AuthService
  ) {}

  selectedId!: string | undefined;

  ngOnInit() {
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
    this.PayService.getAll().subscribe(
      (data) => {
        this.paid = data;
        console.log(this.paid.data);
      },
      (error) => {
        console.log(error);
      }
    );
    this.items = [
      {
        label: 'Favorite',
        icon: 'pi pi-star',
        shortcut: '⌘+D',
      },
      {
        label: 'Add',
        icon: 'pi pi-shopping-cart',
        shortcut: '⌘+A',
      },
      {
        separator: true,
      },
      {
        label: 'Share',
        icon: 'pi pi-share-alt',
        items: [
          {
            label: 'Whatsapp',
            icon: 'pi pi-whatsapp',
            badge: '2',
          },
          {
            label: 'Instagram',
            icon: 'pi pi-instagram',
            badge: '3',
          },
        ],
      },
    ];
  }

  deposit() {
    if (!this.user) {
      alert('Please login to continue');
      return;
    }
    if (!this.amount || this.amount <= 0) {
      alert('Please enter amount to continue');
      return;
    }
    this.paying = true;
    this.timeLeft.time = 180;
    const startTime = new Date().getTime();

    this.startTimeEncoded = Math.floor(startTime / 1000)
      .toString(16)
      .toUpperCase();
    console.log(this.startTimeEncoded);

    this.description = `${this.user.username} ${this.startTimeEncoded}`;
    this.startCountdown();
    this.PayService.createQrCode({
      amount: this.amount,
      addInfo: this.description,
    }).subscribe((data: any) => {
      if (data) {
        this.qrCode = data.data.data.qrDataURL;
      } else {
        alert('Something went wrong');
        return;
      }
    });
    const data: ICreatePayment = {
      userId: this.user._id,
      paymentMethod: 'bank_transfer',
      addInfo: this.description,
      startTime,
      amount: this.amount,
    };
    this.PayService.create(data).subscribe(() => {});
    this.socketService.getSocket().on(this.description, (payment: any) => {
      alert(payment.message);
      this.paying = false;
    });
    setTimeout(() => {
      this.paying = false;
    }, 180000);
  }

  startCountdown() {
    setTimeout(() => {
      this.timeLeft.time = this.timeLeft.time - 1;
      this.timeLeft.h = Math.floor(this.timeLeft.time / 3600);
      this.timeLeft.m = Math.floor((this.timeLeft.time % 3600) / 60);
      this.timeLeft.s = Math.floor((this.timeLeft.time % 3600) % 60);
      this.startCountdown();
    }, 1000);
  }

  onContextMenu(event: any) {
    if (this.cm) {
      this.cm.target = event.currentTarget;
      this.cm.show(event);
    }
  }

  onHide() {
    this.selectedId = undefined;
  }
  data = [
    {
      id: '1000',
      code: 'f230fh0g3',
      name: 'Bamboo Watch',
      description: 'Product Description',
      image: 'bamboo-watch.jpg',
      price: 65,
      category: 'Accessories',
      quantity: 24,
      inventoryStatus: 'INSTOCK',
      rating: 5,
    },
    {
      id: '1001',
      code: 'nvklal433',
      name: 'Black Watch',
      description: 'Product Description',
      image: 'black-watch.jpg',
      price: 72,
      category: 'Accessories',
      quantity: 61,
      inventoryStatus: 'INSTOCK',
      rating: 4,
    },
    {
      id: '1002',
      code: 'zz21cz3c1',
      name: 'Blue Band',
      description: 'Product Description',
      image: 'blue-band.jpg',
      price: 79,
      category: 'Fitness',
      quantity: 2,
      inventoryStatus: 'LOWSTOCK',
      rating: 3,
    },
    {
      id: '1003',
      code: '244wgerg2',
      name: 'Blue T-Shirt',
      description: 'Product Description',
      image: 'blue-t-shirt.jpg',
      price: 29,
      category: 'Clothing',
      quantity: 25,
      inventoryStatus: 'INSTOCK',
      rating: 5,
    },
    {
      id: '1004',
      code: 'h456wer53',
      name: 'Bracelet',
      description: 'Product Description',
      image: 'bracelet.jpg',
      price: 15,
      category: 'Accessories',
      quantity: 73,
      inventoryStatus: 'INSTOCK',
      rating: 4,
    },
  ];
}
