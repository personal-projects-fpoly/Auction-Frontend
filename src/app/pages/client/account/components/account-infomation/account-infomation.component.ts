import { Component } from '@angular/core';
import { IAuth } from 'src/app/interface/auth';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-account-infomation',
  templateUrl: './account-infomation.component.html',
  styleUrl: './account-infomation.component.scss',
})
export class ClientAccountInfomationComponent {
  user: IAuth | null = null;
  constructor(private authService: AuthService) {}
  ngOnInit() {
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
  }
}
