import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAccountInfomationComponent } from './account-infomation.component';

describe('ClientAccountInfomationComponent', () => {
  let component: ClientAccountInfomationComponent;
  let fixture: ComponentFixture<ClientAccountInfomationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientAccountInfomationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientAccountInfomationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
