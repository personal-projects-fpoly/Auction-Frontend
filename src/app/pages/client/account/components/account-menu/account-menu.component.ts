import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import clientAccount from 'src/assets/data/client/menuAccount';

@Component({
  selector: 'app-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrl: './account-menu.component.scss',
})
export class ClientAccountMenuComponent {
  items: MenuItem[] | undefined;
  ngOnInit() {
    this.items = clientAccount;
  }
}
