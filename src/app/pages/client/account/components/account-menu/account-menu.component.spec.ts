import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAccountMenuComponent } from './account-menu.component';

describe('ClientAccountMenuComponent', () => {
  let component: ClientAccountMenuComponent;
  let fixture: ComponentFixture<ClientAccountMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientAccountMenuComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientAccountMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
