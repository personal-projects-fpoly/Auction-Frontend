import { Component, Input } from '@angular/core';
import { IProduct } from '../../../../../interface/product';
import { ISection } from '../../../../../interface/section';

@Component({
  selector: 'app-section-home',
  templateUrl: './section-home.component.html',
  styleUrl: './section-home.component.scss',
})
export class ClientSectionHomeComponent {
  @Input() section: ISection = {} as ISection;
}
