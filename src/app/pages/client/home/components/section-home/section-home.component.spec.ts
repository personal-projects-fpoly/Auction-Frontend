import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSectionHomeComponent } from './section-home.component';

describe('ClientSectionHomeComponent', () => {
  let component: ClientSectionHomeComponent;
  let fixture: ComponentFixture<ClientSectionHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientSectionHomeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientSectionHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
