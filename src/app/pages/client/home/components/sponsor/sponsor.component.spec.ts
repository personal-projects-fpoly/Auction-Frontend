import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSponsorComponent } from './sponsor.component';

describe('ClientSponsorComponent', () => {
  let component: ClientSponsorComponent;
  let fixture: ComponentFixture<ClientSponsorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientSponsorComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
