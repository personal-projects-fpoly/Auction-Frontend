import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSectionNotificationComponent } from './section-notification.component';

describe('ClientSectionNotificationComponent', () => {
  let component: ClientSectionNotificationComponent;
  let fixture: ComponentFixture<ClientSectionNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientSectionNotificationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClientSectionNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
