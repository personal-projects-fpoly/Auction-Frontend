import { Component } from '@angular/core';

@Component({
  selector: 'app-section-notification',
  templateUrl: './section-notification.component.html',
  styleUrl: './section-notification.component.scss',
})
export class ClientSectionNotificationComponent {}
