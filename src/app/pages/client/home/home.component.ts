import { Component } from '@angular/core';
import { ProductService } from '../../../service/product/product.service';
import { IProduct } from '../../../interface/product';
import { ISection } from '../../../interface/section';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class ClientHomeComponent {
  products: IProduct[] = [];
  constructor(private productService: ProductService) {}

  sections: ISection[] = [
    {
      titleSection: 'Tài sản sắp đấu giá',
      titleCol: 'Thời gian bắt đầu',
      products: [],
    },
    {
      titleSection: 'Tài sản đang đấu giá',
      titleCol: 'Thời gian còn lại',
      products: [],
    },
    {
      titleSection: 'Tài sản kết thúc đấu giá',
      titleCol: 'Thời gian kết thúc',
      products: [],
    },
  ];

  ngOnInit(): void {
    this.productService.getProducts().subscribe(
      (data: IProduct[]) => {
        this.products = data;
        this.products.forEach((product) => {
          const currentTime = new Date();
          const startTime = new Date(product.startTime);
          const endTime = new Date(product.endTime);

          if (currentTime < startTime && this.sections[0].products.length < 4) {
            this.sections[0].products.push(product);
          } else if (
            currentTime >= startTime &&
            currentTime <= endTime &&
            this.sections[1].products.length < 4
          ) {
            this.sections[1].products.push(product);
          } else if (this.sections[2].products.length < 4) {
            this.sections[2].products.push(product);
          }
        });

        // Optional: Sort products within each section based on auction times if needed

        console.log(this.sections);
      },
      (error) => {
        console.error('Error fetching products', error);
      }
    );
  }
}
