import { Component } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrl: './checkout.component.scss',
})
export class ClientCheckoutComponent {
  selectedCity: string | undefined;

  cities = [
    { name: 'New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'Istanbul', code: 'IST' },
    { name: 'Paris', code: 'PRS' },
  ];
  countries: any[] | undefined;
  provinces: any[] | undefined;
  selectedProvince: any;
  districts: any[] | undefined;
  selectedDistrict: any;
  wards: any[] | undefined;
  selectedWard: any;
}
