import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminResponsiveHelperComponent } from './responsive-helper.component';

describe('AdminResponsiveHelperComponent', () => {
  let component: AdminResponsiveHelperComponent;
  let fixture: ComponentFixture<AdminResponsiveHelperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminResponsiveHelperComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminResponsiveHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
