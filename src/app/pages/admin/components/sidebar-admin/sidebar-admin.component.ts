import { Component } from '@angular/core';
import { AppJsonService } from '../../../../service/menu/app-json.service';
import { MenuAdminService } from '../../../../service/menu-admin.service';

@Component({
  selector: 'app-sidebar-admin',
  templateUrl: './sidebar-admin.component.html',
  styleUrl: './sidebar-admin.component.scss',
})
export class AdminSidebarComponent {
  constructor(
    public menuService: MenuAdminService,
    public appJson: AppJsonService
  ) {}

  toggleSidebar() {
    this.menuService.showSideBar = !this.menuService.showSideBar;
  }

  toggleMenu(item: any) {
    item.expanded = !item.expanded;
  }
}
