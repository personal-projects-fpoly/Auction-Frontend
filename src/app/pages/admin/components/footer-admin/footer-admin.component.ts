import { Component } from '@angular/core';

@Component({
  selector: 'app-footer-admin',
  templateUrl: './footer-admin.component.html',
  styleUrl: './footer-admin.component.scss',
})
export class AdminFooterComponent {
  public year: number = new Date().getFullYear();
  constructor() {}
  ngOnInit(): void {}
}
