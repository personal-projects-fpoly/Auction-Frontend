import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar-mobile-submenu',
  templateUrl: './navbar-mobile-submenu.component.html',
  styleUrls: ['./navbar-mobile-submenu.component.scss'],
})
export class AdminNavbarMobileSubmenuComponent {}
