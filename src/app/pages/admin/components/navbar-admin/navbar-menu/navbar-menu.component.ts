import { Component } from '@angular/core';
import { MenuAdminService } from '../../../../../service/menu-admin.service';
import pagesMenu from 'src/assets/data/admin/sidebar';

@Component({
  selector: 'app-navbar-menu',
  templateUrl: './navbar-menu.component.html',
  styleUrls: ['./navbar-menu.component.scss'],
})
export class AdminNavbarMenuComponent {
  constructor(public menuService: MenuAdminService) {}

  toggleDropdown(menu: any) {
    menu.expanded = !menu.expanded;
  }

  toggleSubmenu(item: any) {
    item.expanded = !item.expanded;
  }
}
