import { Component } from '@angular/core';
@Component({
  selector: 'div[navbar-submenu]',
  templateUrl: './navbar-submenu.component.html',
  styleUrls: ['./navbar-submenu.component.scss'],
})
export class AdminNavbarSubmenuComponent {}
