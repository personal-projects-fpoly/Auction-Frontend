import { Component } from '@angular/core';
import { CategoryService } from '../../../../service/category/category.service';
import { ICategory } from '../../../../interface/category';
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrl: './category-list.component.scss',
})
export class AdminCategoryListComponent {
  categories: ICategory[] = [];

  constructor(private categoryService: CategoryService) {}

  ngOnInit() {
    this.categoryService.getAll().subscribe((data: ICategory[]) => {
      this.categories = data;
      console.log(data);
    });
  }

  deleteCategory(id: string) {
    console.log(id);
    this.categoryService.delete(id as string).subscribe(() => {
      this.categories = this.categories.filter(
        (category) => category._id !== id
      );
    });
  }
}
