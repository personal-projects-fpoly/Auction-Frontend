import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../../../service/category/category.service';
import { ICategory } from '../../../../interface/category';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrl: './category-add.component.scss',
})
export class AdminCategoryAddComponent {
  constructor(private categryS: CategoryService) {}

  categoryForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
  });

  onSubmit(): void {
    if (this.categoryForm.valid) {
      console.log(this.categoryForm.value);

      this.categryS.add(this.categoryForm.value as ICategory).subscribe(
        (res) => {
          console.log(res);
          alert('Category added successfully');
          this.categoryForm.reset();
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      console.log('Form is invalid');
    }
  }
}
