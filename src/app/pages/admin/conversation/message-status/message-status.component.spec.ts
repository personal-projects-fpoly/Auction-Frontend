import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageStatusComponent } from './message-status.component';

describe('AdminMessageStatusComponent', () => {
  let component: AdminMessageStatusComponent;
  let fixture: ComponentFixture<AdminMessageStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageStatusComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
