import { ActivatedRoute } from '@angular/router';
import { MessageService } from './../../../../service/message/message.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-message-status',
  templateUrl: './message-status.component.html',
  styleUrl: './message-status.component.scss',
})
export class AdminMessageStatusComponent {
  conversations: any[] = [];
  id: string = '';

  constructor(
    private messageService: MessageService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.loadConversations();
    });
  }

  loadConversations() {
    this.messageService.getByStatus(this.id).subscribe(
      (data) => {
        this.conversations = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
