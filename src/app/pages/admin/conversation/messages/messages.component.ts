import { MessageService } from './../../../../service/message/message.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrl: './messages.component.scss',
})
export class AdminMessagesComponent {
  constructor(private MessageService: MessageService) {}
  conversations: any[] = [];
  ngOnInit() {
    this.MessageService.getAll().subscribe(
      (data) => {
        this.conversations = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
