import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageListComponent } from './message-list.component';

describe('AdminMessageListComponent', () => {
  let component: AdminMessageListComponent;
  let fixture: ComponentFixture<AdminMessageListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
