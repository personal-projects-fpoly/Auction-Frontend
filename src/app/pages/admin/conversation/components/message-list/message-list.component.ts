import { IConversation } from './../../../../../interface/conversation';
import { DatePipe } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrl: './message-list.component.scss',
})
export class AdminMessageListComponent {
  @Input() conversations: IConversation[] = [];

  constructor(private datePipe: DatePipe) {}
  formatTime(timestamp: Date) {
    return this.datePipe.transform(timestamp, 'shortTime');
  }
}
