import { Component } from '@angular/core';

@Component({
  selector: 'app-message-menu',
  templateUrl: './message-menu.component.html',
  styleUrls: ['./message-menu.component.scss'],
})
export class AdminMessageMenuComponent {
  items: any[] | undefined;

  ngOnInit() {
    this.items = [
      {
        separator: true,
      },
      {
        label: 'My Email',
        items: [
          {
            label: 'Hộp thư',
            icon: { name: 'mail' },
            path: '/admin/conversation',
            badge: '2',
          },
          {
            label: 'Gắn sao',
            icon: { name: 'star' },
            path: '/admin/conversation/star',
            badge: '2',
          },
          {
            label: 'Bản nháp',
            icon: { name: 'pencil' },
            path: '/admin/conversation/category/draft',
            badge: '2',
          },
          {
            label: 'Quan trọng',
            icon: { name: 'mail-warning' },
            path: '/admin/conversation/status/important',
            badge: '2',
          },
          {
            label: 'Thư rác',
            icon: { name: 'mail-question' },
            path: '/admin/conversation/status/spam',
            badge: '2',
          },
          {
            label: 'Thùng rác',
            icon: { name: 'trash-2' },
            path: '/admin/conversation/status/deleted',
            badge: '2',
          },
        ],
      },
      {
        label: 'Nhãn',
        items: [
          {
            label: 'Dịch Vụ',
            icon: { name: 'mail', color: '#00B69B' },
            path: '/admin/conversation/label/service',
          },
          {
            label: 'Phản Hồi',
            icon: { name: 'mail', color: '#7D3A89' },
            path: '/admin/conversation/label/feedback',
            badge: '2',
          },
          {
            label: 'Đơn Hàng',
            icon: { name: 'mail', color: '#FD9A56' },
            path: '/admin/conversation/label/order',
            badge: '2',
          },
        ],
      },
      {
        separator: true,
      },
    ];
  }
}
