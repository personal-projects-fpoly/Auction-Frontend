import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageMenuComponent } from './message-menu.component';

describe('AdminMessageMenuComponent', () => {
  let component: AdminMessageMenuComponent;
  let fixture: ComponentFixture<AdminMessageMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageMenuComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
