import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageTopbarComponent } from './message-topbar.component';

describe('AdminMessageTopbarComponent', () => {
  let component: AdminMessageTopbarComponent;
  let fixture: ComponentFixture<AdminMessageTopbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageTopbarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
