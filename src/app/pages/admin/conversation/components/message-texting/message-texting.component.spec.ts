import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageTextingComponent } from './message-texting.component';

describe('AdminMessageTextingComponent', () => {
  let component: AdminMessageTextingComponent;
  let fixture: ComponentFixture<AdminMessageTextingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageTextingComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageTextingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
