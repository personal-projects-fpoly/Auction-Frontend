import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IMessage } from '../../../../../interface/message';
import { SocketOiService } from '../../../../../service/socket-oi/socket-oi.service';
import { MessageService } from '../../../../../service/message/message.service';
import { IConversation } from '../../../../../interface/conversation';
import { ActivatedRoute } from '@angular/router';
import { IAuth } from 'src/app/interface/auth';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-message-texting',
  templateUrl: './message-texting.component.html',
  styleUrl: './message-texting.component.scss',
})
export class AdminMessageTextingComponent {
  conversation: any = {};
  @ViewChild('scrollMessage') chatContainer: ElementRef | undefined;
  texting = false;
  message: string = '';
  userId: string = '';
  user: IAuth | null = null;

  formMessage = new FormGroup({
    message: new FormControl('', Validators.required),
  });

  constructor(
    private socketService: SocketOiService,
    private messageService: MessageService,
    private router: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.userId = this.router.snapshot.params['id'];
    this.authService.userValue.subscribe((data) => {
      this.user = data;
    });
    this.messageService.getByUser(this.userId).subscribe(
      (data: IConversation) => {
        this.conversation = data;
        if (this.conversation._id) {
          this.socketService
            .getSocket()
            .on(this.conversation._id, (data: IMessage) => {
              console.log('Message update:', data);
              this.conversation.messages.push(data);
              this.scrollToBottom();
            });
        }
      },
      () => {
        this.messageService
          .add({
            userId: this.userId,
          })
          .subscribe((data: IConversation) => {
            this.conversation = data;
            console.log(data);
          });
      }
    );
    this.scrollToBottom();
  }
  sendMessage() {
    if (this.formMessage.valid && this.user) {
      const newMessage: any = {
        sender: {
          senderId: this.user._id,
          name: this.user.name,
          avatar: 'https://picsum.photos/200/300',
        },
        content: this.formMessage.value.message,
        status: 'sent',
      };

      this.messageService
        .update(this.userId, newMessage)
        .subscribe((data: IMessage) => {
          console.log('Update success');
          this.formMessage.reset();
          this.texting = false;
          this.scrollToBottom();
        });
    }
  }
  handleMessage(event: any) {
    this.message = event.target.value;
    if (this.message) {
      this.texting = true;
    } else {
      this.texting = false;
    }
  }
  ngAfterViewChecked() {
    this.scrollToBottom();
  }
  private scrollToBottom(): void {
    if (this.chatContainer) {
      const container = this.chatContainer.nativeElement;
      container.scrollTop = container.scrollHeight;
    }
  }
  // private playNotificationSound(): void {
  //   const sound = new Audio(notificationSound);
  //   sound.play().catch((error) => console.error('Error playing sound:', error));
  // }
}
