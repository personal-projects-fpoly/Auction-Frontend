import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageLabelComponent } from './message-label.component';

describe('AdminMessageLabelComponent', () => {
  let component: AdminMessageLabelComponent;
  let fixture: ComponentFixture<AdminMessageLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageLabelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
