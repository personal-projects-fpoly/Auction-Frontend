import { ActivatedRoute } from '@angular/router';
import { MessageService } from './../../../../service/message/message.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-label',
  templateUrl: './message-label.component.html',
  styleUrls: ['./message-label.component.scss'],
})
export class AdminMessageLabelComponent implements OnInit {
  conversations: any[] = [];
  id: string = '';

  constructor(
    private messageService: MessageService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.loadConversations();
    });
  }

  loadConversations() {
    this.messageService.getByLabel(this.id).subscribe(
      (data) => {
        this.conversations = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
