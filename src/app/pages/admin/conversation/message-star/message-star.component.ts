import { MessageService } from './../../../../service/message/message.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-message-star',
  templateUrl: './message-star.component.html',
  styleUrl: './message-star.component.scss',
})
export class AdminMessageStarComponent {
  constructor(private MessageService: MessageService) {}
  conversations: any[] = [];
  ngOnInit() {
    this.MessageService.getByStar().subscribe(
      (data) => {
        this.conversations = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
