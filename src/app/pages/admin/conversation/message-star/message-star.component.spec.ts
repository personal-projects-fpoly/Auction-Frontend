import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMessageStarComponent } from './message-star.component';

describe('AdminMessageStarComponent', () => {
  let component: AdminMessageStarComponent;
  let fixture: ComponentFixture<AdminMessageStarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMessageStarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AdminMessageStarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
