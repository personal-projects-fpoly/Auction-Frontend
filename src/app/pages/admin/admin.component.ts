import { SocketOiService } from './../../service/socket-oi/socket-oi.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  bid: any = {};
  constructor(
    private messageService: MessageService,
    private SocketOiService: SocketOiService,
    private router: Router
  ) {
    this.SocketOiService.getSocket().on('updateProduct', (data: any) => {
      console.log('Received bid update:', data);
      this.bid = data;
      console.log(this.bid);

      this.showConfirm();
    });
  }

  showConfirm() {
    this.messageService.add({
      key: 'confirm',
      severity: 'info',
      summary: 'New Order',
      detail: 'Có đơn hàng mới của khách nè !',
      life: 3000,
    });
  }

  onConfirm() {
    this.router.navigate([
      `/admin/conversation/texting/${this.bid.bids[0].user}`,
    ]);
  }

  onReject() {
    // Logic for the onClose event
    console.log('Toast closed!');
  }
}
