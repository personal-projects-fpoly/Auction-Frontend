import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../../../service/product/product.service';
import { UploadService } from './../../../../../service/upload/upload.service';
import { CategoryService } from '../../../../../service/category/category.service';
import { ICategory } from '../../../../../interface/category';
import { catchError, forkJoin, map, mergeMap, of } from 'rxjs';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss'],
})
export class AdminProductAddComponent {
  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private uploadService: UploadService
  ) {}

  categories: ICategory[] = [];
  thumbnailFile: File | null = null;
  thumbnailFileUrl: string | null = null;
  imagesFiles: File[] = [];
  imagesFilesUrls: string[] = [];
  thumbnailError: boolean = false;
  imagesError: boolean = false;

  formProduct = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    shortDescription: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    category: new FormControl('', Validators.required),
    startPrice: new FormControl('', Validators.required),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
  });
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    // upload: (file: File) => { ... }
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
  };
  ngOnInit() {
    this.categoryService.getAll().subscribe((data: ICategory[]) => {
      this.categories = data;
    });
  }

  onThumbnailSelected(event: any) {
    this.thumbnailFile = event.target.files[0];
    if (this.thumbnailFile) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.thumbnailFileUrl = e.target.result;
      };
      reader.readAsDataURL(this.thumbnailFile);
      this.thumbnailError = false;
    }
  }

  onImagesSelected(event: any) {
    this.imagesFiles = [];
    this.imagesFilesUrls = [];
    const files = Array.from(event.target.files) as File[];
    if (files.length > 4) {
      this.imagesError = true;
    } else {
      files.forEach((file) => {
        this.imagesFiles.push(file);
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.imagesFilesUrls.push(e.target.result);
        };
        reader.readAsDataURL(file);
      });
      this.imagesError = false;
    }
  }
  onSubmit() {
    if (
      !this.thumbnailFile ||
      this.imagesFiles.length === 0 ||
      this.formProduct.invalid
    ) {
      this.thumbnailError = !this.thumbnailFile;
      this.imagesError = this.imagesFiles.length === 0;
      return;
    }
    const startTime = this.formProduct.value.startTime ?? '';
    const endTime = this.formProduct.value.endTime ?? '';

    if (new Date(startTime) > new Date(endTime)) {
      this.imagesError = true;
      alert('Ngày bắt đầu phải trước ngày kết thúc');
      return;
    }
    this.uploadService
      .upload(this.thumbnailFile as File)
      .pipe(
        mergeMap((res: any) => {
          const thumbnailFileName = res.url;
          return forkJoin(
            this.imagesFiles.map((file) =>
              this.uploadService.upload(file as File).pipe(
                map((response: any) => response.url),
                catchError((error) => {
                  console.log(error);
                  return of(''); // Return an empty string in case of error
                })
              )
            )
          ).pipe(
            map((imagesFileNames: string[]) => {
              imagesFileNames.unshift(thumbnailFileName); // Add thumbnail to the beginning of the array
              return imagesFileNames;
            })
          );
        })
      )
      .subscribe(
        (imagesFileNames: string[]) => {
          this.productService
            .addProduct({
              name: this.formProduct.value.name,
              shortDescription: this.formProduct.value.shortDescription,
              description: this.formProduct.value.description,
              category: this.formProduct.value.category,
              startPrice: this.formProduct.value.startPrice,
              startTime: this.formProduct.value.startTime,
              endTime: this.formProduct.value.endTime,
              thumbnail: imagesFileNames[0],
              images: imagesFileNames,
            })
            .subscribe((res: any) => {
              console.log(res);
              alert('Product added successfully');
              window.location.reload();
            });
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
