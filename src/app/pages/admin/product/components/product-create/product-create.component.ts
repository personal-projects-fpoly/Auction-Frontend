import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../../../service/product/product.service';
import { UploadService } from '../../../../../service/upload/upload.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrl: './product-create.component.scss',
})
export class AdminProductCreateComponent {
  constructor(
    private productService: ProductService,
    private UploadService: UploadService
  ) {}
  thumbnailFile: File | null = null;
  thumbnailFileUrl: string | null = null;
  imagesFiles: File[] = [];
  imagesFilesUrls: string[] = [];
  thumbnailError: boolean = false;
  imagesError: boolean = false;
  formProduct = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    shortDescription: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    category: new FormControl('', Validators.required),
    thumbnail: new FormControl(null, Validators.required),
    images: new FormControl([]),
    startPrice: new FormControl('', Validators.required),
    startTime: new FormControl(''),
    endTime: new FormControl(''),
  });
  onThumbnailSelected(event: any) {
    this.thumbnailFile = event.target.files[0];
    if (this.thumbnailFile) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.thumbnailFileUrl = e.target.result;
      };
      reader.readAsDataURL(this.thumbnailFile);
    }
  }

  onImagesSelected(event: any) {
    this.imagesFiles = [];
    this.imagesFilesUrls = [];
    const files = Array.from(event.target.files);
    if (files.length > 4) {
      this.imagesError = true;
    } else {
      for (let i = 0; i < event.target.files.length; i++) {
        const file = event.target.files[i];
        this.imagesFiles.push(file);
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.imagesFilesUrls.push(e.target.result);
        };
        reader.readAsDataURL(file);
      }
    }
  }

  onSubmit() {
    if (
      !this.thumbnailFile ||
      this.imagesFiles.length === 0 ||
      this.formProduct.invalid
    ) {
      this.thumbnailError = !this.thumbnailFile;
      this.imagesError = this.imagesFiles.length === 0;
      return;
    }
    console.log(this.formProduct.value);
    this.productService
      .addProduct(this.formProduct.value)
      .subscribe((res: any) => {});
  }
}
