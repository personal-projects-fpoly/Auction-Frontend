import { Component } from '@angular/core';
import { IProduct } from '../../../../../interface/product';
import { ProductService } from '../../../../../service/product/product.service';
import { CategoryService } from '../../../../../service/category/category.service';
import { ICategory } from '../../../../../interface/category';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.scss',
})
export class AdminProductListComponent {
  products: IProduct[] = [];
  categories: ICategory[] = [];

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.productService.getProducts().subscribe(
      (data: IProduct[]) => {
        this.products = data;
      },
      (error) => {
        console.error('Error fetching products', error);
      }
    );

    this.categoryService.getAll().subscribe(
      (data: ICategory[]) => {
        this.categories = data;
      },
      (error) => {
        console.error('Error fetching categories', error);
      }
    );
  }
  onDelete(productId: string | undefined): void {
    if (productId) {
      if (confirm('Are you sure you want to delete this product?')) {
        this.productService.deleteProduct(productId).subscribe(
          () => {
            this.products = this.products.filter(
              (product) => product._id !== productId
            );
          },
          (error) => {
            console.error('Error deleting product', error);
          }
        );
      }
    }
  }

  getCategoryName(categoryId: string | undefined): string | undefined {
    if (categoryId) {
      return this.categories.find((category) => category._id === categoryId)
        ?.name;
    }
    return undefined;
  }
}
