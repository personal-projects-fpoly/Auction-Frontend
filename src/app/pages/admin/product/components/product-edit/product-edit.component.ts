import { Component } from '@angular/core';
import { ProductService } from '../../../../../service/product/product.service';
import { CategoryService } from '../../../../../service/category/category.service';
import { UploadService } from '../../../../../service/upload/upload.service';
import { ICategory } from '../../../../../interface/category';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { catchError, forkJoin, map, mergeMap, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrl: './product-edit.component.scss',
})
export class AdminProductEditComponent {
  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private uploadService: UploadService,
    private route: ActivatedRoute
  ) {}

  productId: string = '';
  categories: ICategory[] = [];
  thumbnailFile: File | null = null;
  thumbnailFileUrl: string | null = null;
  imagesFiles: File[] = [];
  imagesFilesUrls: string[] = [];
  thumbnailError: boolean = false;
  imagesError: boolean = false;

  formProduct = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    shortDescription: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    category: new FormControl('', Validators.required),
    startPrice: new FormControl('', Validators.required),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
  });
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    // upload: (file: File) => { ... }
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
  };
  ngOnInit() {
    this.categoryService.getAll().subscribe((data: ICategory[]) => {
      this.categories = data;
    });

    this.productId = this.route.snapshot.params['id'];
    this.productService.getProduct(this.productId).subscribe((data) => {
      this.formProduct.patchValue({
        name: data.name,
        shortDescription: data.shortDescription,
        description: data.description,
        category: data.category,
        startPrice: data.startPrice,
        startTime: data.startTime,
        endTime: data.endTime,
      });
    });
  }

  onThumbnailSelected(event: any) {
    this.thumbnailFile = event.target.files[0];
    if (this.thumbnailFile) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.thumbnailFileUrl = e.target.result;
      };
      reader.readAsDataURL(this.thumbnailFile);
      this.thumbnailError = false;
    }
  }

  onImagesSelected(event: any) {
    this.imagesFiles = [];
    this.imagesFilesUrls = [];
    const files = Array.from(event.target.files) as File[];
    if (files.length > 4) {
      this.imagesError = true;
    } else {
      files.forEach((file) => {
        this.imagesFiles.push(file);
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.imagesFilesUrls.push(e.target.result);
        };
        reader.readAsDataURL(file);
      });
      this.imagesError = false;
    }
  }
  onSubmit() {
    if (this.formProduct.invalid) {
      return;
    }
    const startTime = this.formProduct.value.startTime ?? '';
    const endTime = this.formProduct.value.endTime ?? '';

    if (new Date(startTime) > new Date(endTime)) {
      this.imagesError = true;
      alert('Ngày bắt đầu phải trước ngày kết thúc');
      return;
    }
    let thumbnailUpload$ = of('');
    if (this.thumbnailFile) {
      thumbnailUpload$ = this.uploadService
        .upload(this.thumbnailFile as File)
        .pipe(
          map((res: any) => res.url),
          catchError((error) => {
            console.log(error);
            return of('');
          })
        );
    }

    thumbnailUpload$
      .pipe(
        mergeMap((thumbnailUrl: string) => {
          const imagesUpload$ =
            this.imagesFiles.length > 0
              ? forkJoin(
                  this.imagesFiles.map((file) =>
                    this.uploadService.upload(file as File).pipe(
                      map((response: any) => response.url),
                      catchError((error) => {
                        console.log(error);
                        return of('');
                      })
                    )
                  )
                )
              : of([]);
          return imagesUpload$.pipe(
            map((imagesFileNames: string[]) => {
              if (thumbnailUrl) {
                imagesFileNames.unshift(thumbnailUrl);
              }
              return imagesFileNames;
            })
          );
        })
      )
      .subscribe(
        (imagesFileNames: string[]) => {
          this.productService
            .updateProduct(this.productId, {
              name: this.formProduct.value.name,
              shortDescription: this.formProduct.value.shortDescription,
              description: this.formProduct.value.description,
              category: this.formProduct.value.category,
              startPrice: this.formProduct.value.startPrice,
              startTime: this.formProduct.value.startTime,
              endTime: this.formProduct.value.endTime,
              thumbnail: imagesFileNames[0],
              images: imagesFileNames.slice(1),
            })
            .subscribe((res: any) => {
              alert('Product edited successfully');
            });
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
