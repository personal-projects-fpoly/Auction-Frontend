import { Component } from '@angular/core';
import { ProductService } from '../../../service/product/product.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
})
export class AdminDashboardComponent {
  ongoingAuctions: number = 0;
  totalBids: number = 0;
  registeredUsers: number = 0;

  constructor(private productService: ProductService) {}

  // ngOnInit(): void {
  //   // Replace with actual service calls to fetch data
  //   this.fetchOngoingAuctions();
  //   this.fetchTotalBids();
  //   this.fetchRegisteredUsers();
  // }

  // fetchOngoingAuctions() {
  //   // Example service call
  //   this.productService.getOngoingAuctions().subscribe((data) => {
  //     this.ongoingAuctions = data.length;
  //   });
  // }

  // fetchTotalBids() {
  //   // Example service call
  //   this.productService.getTotalBids().subscribe((data) => {
  //     this.totalBids = data.total;
  //   });
  // }

  // fetchRegisteredUsers() {
  //   // Example service call
  //   this.productService.getRegisteredUsers().subscribe((data) => {
  //     this.registeredUsers = data.total;
  //   });
  // }
}
