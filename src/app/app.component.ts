import { Component } from '@angular/core';
import { AuthService } from './service/auth/auth.service';
import { IAuth } from './interface/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'autionsho';
  user: IAuth | null = null;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    if (this.authService.userValue) {
      this.authService.protectedAuth().subscribe(
        (data) => {
          this.user = data;
        },
        (error) => {
          console.error('Error fetching protected data:', error);
        }
      );
    }
  }
}
