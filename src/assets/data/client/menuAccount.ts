const clientAccount = [
  {
    separator: true,
  },
  {
    items: [
      {
        label: 'Ví cá nhân',
        icon: 'pi pi-plus',
        shortcut: '⌘+N',
        path: 'deposit',
      },
      {
        label: 'Tìm kiếm',
        icon: 'pi pi-search',
        shortcut: '⌘+S',
        path: 'deposit',
      },
      {
        label: 'Quản trị',
        icon: 'pi pi-cog',
        shortcut: '⌘+O',
        path: 'deposit',
      },
      {
        label: 'Tin nhắn',
        icon: 'pi pi-inbox',
        badge: '2',
        path: 'deposit',
      },
      {
        label: 'Đăng xuất',
        icon: 'pi pi-sign-out',
        shortcut: '⌘+Q',
        path: 'deposit',
      },
    ],
  },
];

export default clientAccount;
