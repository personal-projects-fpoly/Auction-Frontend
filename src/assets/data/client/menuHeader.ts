import { ClientMenuItem } from 'src/app/interface/menuBar';

const Navs: ClientMenuItem[] = [
  {
    label: 'Tài Sản Đấu Giá',
    items: [
      {
        label: 'Tài Sản Nhà Nước',
        url: 'category',
      },
      {
        label: 'Bất Động Sản',
        url: 'category',
      },
      {
        label: 'Phương tiện - Xe Cộ',
        url: 'category',
      },
      {
        label: 'Sưu tầm - nghệ thuật',
        url: 'category',
      },
      {
        label: 'Hàng hiệu xa xỉ',
        url: 'category',
      },
      {
        label: 'Tang vật tịch thu',
        url: 'category',
      },
      {
        label: 'Tài sản khác',
        url: 'category',
      },
    ],
  },
  {
    label: 'Cuộc đấu giá',
    items: [
      {
        label: 'Cuộc đấu giá sắp đấu giá',
        url: 'list',
      },
      {
        label: 'Cuộc đấu giá đang diễn ra',
        url: 'list',
      },
      {
        label: 'Cuộc đấu giá đã kết thúc',
        url: 'list',
      },
    ],
  },
  {
    label: 'Tin tức',
    items: [
      {
        label: 'Thông báo',
        url: 'news',
      },
      {
        label: 'Thông báo đấu giá',
        url: 'news',
      },
      {
        label: 'Tin khác',
        url: 'news',
      },
    ],
  },
  {
    label: 'Giới Thiệu',
    url: 'about',
  },
  {
    label: 'Liên hệ',
    url: 'contact',
  },
];
export default Navs;
