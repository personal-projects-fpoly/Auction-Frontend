import { AdminMenuGroup } from './../../../app/interface/navAdmin';
const pagesMenu: AdminMenuGroup[] = [
  {
    group: 'Dashboard',
    items: [
      {
        label: 'Statistical',
        route: '/statistical',
        icon: 'pie-chart',
        active: false,
        expanded: false,
      },
      {
        label: 'Product',
        route: '/product',
        icon: 'package-search',
        active: false,
        expanded: false,
        children: [
          {
            label: 'Product List',
            route: '',
            active: false,
            expanded: false,
          },
          {
            label: 'Product Add',
            route: '/add',
            active: false,
            expanded: false,
          },
        ],
      },
      {
        label: 'Category',
        route: '/category',
        icon: 'layers-3',
        active: false,
        expanded: false,
        children: [
          {
            label: 'Category List',
            route: '',
            active: false,
            expanded: false,
          },
          {
            label: 'Category Add',
            route: '/add',
            active: false,
            expanded: false,
          },
        ],
      },
      {
        label: 'Conversation',
        route: '/conversation',
        icon: 'message-square-text',
        active: false,
        expanded: false,
      },
    ],
    separator: true,
    expanded: undefined,
    selected: undefined,
    active: undefined
  },
];

export default pagesMenu;
